<?php
/*
Plugin Name: Custom Region
Plugin URI: 
Description: Custom region( Area ). Use : CustomRegion::region_custom_list_categories();
Author: Nguyen Minh Son
Author URI: 
Text Domain: custom-region
Domain Path: 
Version: 1.0
*/
/* Start Adding Functions Below this Line */
define('CUSTOM_PLUGIN_VERSION','1.0');
define('CUSTOM_REGION_PATH', dirname(__FILE__));
/*
CUSTOM COUNTRY,CITY,TOWN
----------------------------------*/
class CustomRegion{
	private static $instance;
	public static function get_instance()
	{
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
	}

	public function __construct()
	{
		$this->post_name = 'Custom Region';
		$this->post_slug = 'custom-region';
		// Hook into the 'init' action
		add_action( 'init', array($this,'init_post'), 0 );
		add_action( 'init', array($this,'init_post_taxonomies'), 0 );	
	}

	public function init_post() {

		$labels = array(
			'name'                => _x( $this->post_name, 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( $this->post_name, 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( $this->post_name, 'text_domain' ),
			'parent_item_colon'   => __( $this->post_name . ' parent', 'text_domain' ),
			'all_items'           => __( 'All ' . $this->post_name, 'text_domain' ),
			'view_item'           => __( 'View ' ),
			'add_new_item'        => __( 'Add ' . $this->post_name, 'text_domain' ),
			'add_new'             => __( 'Add new ' . $this->post_name, 'text_domain' ),
			'edit_item'           => __( 'Edit ' . $this->post_name, 'text_domain' ),
			'update_item'         => __( 'Update ' . $this->post_name, 'text_domain' ),
			'search_items'        => __( 'Search ' . $this->post_name, 'text_domain' ),
			'not_found'           => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'  => __( 'Not found in trash', 'text_domain' ),
		);
		$args = array(
			'label'               => __( $this->post_name, 'text_domain' ),
			'description'         => __( $this->post_name, 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array(  'title', 'editor' ),
			'taxonomies'          => array( $this->post_slug ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			//'menu_position'       => 5,
			//'menu_icon'           => 'post',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( $this->post_slug, $args );
	}


	function init_post_taxonomies() {
	  // Add new "Locations" taxonomy to Posts
	  register_taxonomy( 'c-'.$this->post_slug , array( $this->post_slug ), array(
	    // Hierarchical taxonomy (like categories)
	    'hierarchical' => true,
	    // This array of options controls the labels displayed in the WordPress Admin UI
	    'labels' => array(
	      'name' => _x( 'Category ' . $this->post_name, 'taxonomy general name' ),
	      'singular_name' => _x( 'Category ' . $this->post_name, 'taxonomy singular name' ),
	      'search_items' =>  __( 'Search Category ' . $this->post_name ),
	      'all_items' => __( 'All Category ' . $this->post_name ),
	      'parent_item' => __( 'Category parent ' . $this->post_name ),
	      'parent_item_colon' => __( 'Category parent ' . $this->post_name . ':' ),
	      'edit_item' => __( 'Edit Category ' . $this->post_name ),
	      'update_item' => __( 'Update Category ' . $this->post_name ),
	      'add_new_item' => __( 'Add Category ' . $this->post_name ),
	      'new_item_name' => __( 'New Category ' . $this->post_name),
	      'menu_name' => __( 'Category ' . $this->post_name ),
	    ),	    
	    // Control the slugs used for this taxonomy
	    'rewrite' => array(
	      'slug' => 'c-'.$this->post_slug, // This controls the base slug that will display before each term
	      //'with_front' => false, // Don't display the category base before "/$post_slug/"
	      'hierarchical' => true // This will allow URL's like "/$post_slug/boston/cambridge/"
	    ),
	  ));
	}

	// display
	public function get_region_panel()
	{		
        $terms = get_terms('c-'.$this->post_slug,array(
	        'orderby' => 'id',
	        'order' => 'ASC',
	        'hide_empty'    => false,
        ));          
		return $this->region_custom_list_categories($terms);	
	}

	function region_custom_list_posts($categories, $parent)
	{		
		$count = 0;
		foreach($categories as $category){		
			if($category->parent == $parent){
				$count++;
			}
		}		
		//no sub category: display post

		if($count == 0){
		    $posts = get_posts(array(
		    	'post_type' => $this->post_slug,
				'posts_per_page' => -1,
				'orderby' => 'id',
				'order' => 'asc',
				'tax_query' => array(
					array(
						'taxonomy' => 'c-' . $this->post_slug,
						'field' => 'term_id',
						'terms' => $parent
					)
				)		    	
		    ));		    
		    
		    wp_reset_query();
		}
		if(!empty($posts)){
			$output = '<ul class="region_post_list">';
			foreach($posts as $p){				
				$output .= '<li><a href="' . get_permalink($p->ID) . '">' . $p->post_title . '</a></li>';
			}
			$output .= '</ul>';
			return $output;
		}
		return '';
	}

	function region_custom_list_categories($categories, $parent = 0, $depth = 0, $current_depth = 1)
	{
		$output = '';
		if($depth != 0 && $current_depth > $depth){
			return $output;
		}
		if($current_depth > 1){
			$output .= '<ul>';
		}	
		foreach($categories as $category){		
			if($category->parent == $parent){
				$output .= '<li class="region-category">';				
				$output .=  '<h3 class="area-title">' . $category->name;			
				$output .= "</h3>";				
				// find child
				$output .= $this->region_custom_list_categories($categories,$category->term_id, $depth, $current_depth+1);
				$output .= $this->region_custom_list_posts($categories, $category->term_id);
				$output .= '</li>';						
			}
			//print_r($category);
		}	
		if($current_depth > 1){
			$output .= '</ul>';
		}
		return $output;
	}	
}



CustomRegion::get_instance();