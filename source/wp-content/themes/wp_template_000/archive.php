<?php get_header(); ?>                   
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
        <div class="primary-row">            
            <h2 class ="h2-title"><?php the_title(); ?></h2>            
            <div class="post-row-content">                
                <div class="post-row-meta">
                    <i class="fa fa-clock-o"><?php the_time('l, F jS, Y'); ?></i>
                    <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                    <i class="fa fa-user"></i><?php the_author_link(); ?>
                </div>
                <div class="post-row-description"><?php the_content(); ?></div>                    
            </div>
        </div>
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
<?php get_footer(); ?>