<?php get_template_part('header'); ?>
<div class="primary-row clearfix">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<h2 class="h2-title"><?php the_title(); ?></h2>
	<div class="post-row-content">
		<div class="post-row-meta">
			<i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
			<i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
			<i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
		</div>    
    	<div class="blog-content"><?php the_content(); ?></div>
    </div>    
	<?php endwhile; endif; ?>
       <!-- post navigation -->
    <div class="navigation">
		<?php if( get_previous_post() ): ?>
		<div style="float:left;"><?php previous_post_link('%link', '« %title'); ?></div>
		<?php endif;
		if( get_next_post() ): ?>
		<div style="float:right;"><?php next_post_link('%link', '%title »'); ?></div>
		<?php endif; ?>
    </div>
    <!-- /post navigation -->	
</div>
<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->

<?php get_template_part('footer'); ?>