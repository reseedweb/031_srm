<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">工事実績</h2>
	<p>「原状回復おまかせ.com」の工事実績です。</p>
	<p>掲載可能な実績のみ掲載させていただいております。</p>
	<?php
	    $queried_object = get_queried_object();
	    //print_r($queried_object);
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> 'work',
	        'posts_per_page' => 4,
	        'paged' => get_query_var('paged'),
			//'orderby'           => 'slug', 
			//'order'             => 'ASC',  
	    ));
	    ?>
	<div class="work-group"><!-- begin work-group -->
		<?php $i = 0; ?>
	    <?php foreach($posts as $p) :  ?>
	        <?php $i++; ?>
	        <?php if($i%2 == 1) : ?>	
        <div class="work-row clearfix"><!--work-row -->
		<?php endif; ?>     
            <div class="work-col work-col370"><!-- begin work-col-->                
                <div class="image">
                    <a href="<?php echo get_the_permalink($p->ID); ?>">
						<?php echo get_the_post_thumbnail( $p->ID,'large'); ?>                        
					</a>
                </div><!-- ./image --> 
				<h3 class="work-title"><a href="<?php echo get_the_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></h3>				
                <div class="work-text clearfix">
                    <div class="wtext1">竣工: <?php echo get_the_date('Y', $p->ID); ?> 年</div>
					<div class="wtext2">概要: <?php @the_terms($p->ID, 'cat-work'); ?></div>					 
                </div><!-- ./text -->
				<div class="work-rm">
					<a href="<?php echo get_the_permalink($p->ID); ?>">
						→ 詳しく見る
					</a>
				</div><!-- ./work-rm -->
            </div><!-- end work-col -->                
			 <?php if($i%2 == 0 || $i == count($posts) ) : ?>
        </div><!-- end work-row -->	
		<?php endif; ?>
	    <?php endforeach; ?>		
    </div><!-- end work-group -->
	
	<div class="primary-row text-center">
	    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
	<?php wp_reset_query(); ?>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>