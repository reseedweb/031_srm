<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">会社概要</h2>
		<table class="company-table"><!-- begin company-table -->
			<tr>
				<th>会社名</th>
				<td>シンエイ・リテイルマネジメント株式会社</td>
			</tr>
			<tr>
				<th>代表取締役</th>
				<td>金子　皓一</td>
			</tr>
			<tr>
				<th>営業拠点</th>
				<td>
					◆東京オフィス（関東支社）<br />
					　〒140-0015　東京都品川区西大井1-1-2　Ｊタワー西大井イースト2F<br />
					　TEL：（代表）03-5718-4681　 FAX：03-3774-2172<br />
					<p class="pt20">◆名古屋オフィス（中部支社/本社）<br />
					　〒466-0059　名古屋市昭和区福江2-9-33　 navi白金4Ｆ<br />
					　TEL：（代表）052-871-1781　 FAX：052-871-1782
					</p>
					<p class="pt20">◆メンテナンスセンター「365日24ｈ対応」<br />
					　TEL：052-508-4621（契約企業様専用）
					</p>
				</td>
			</tr>
			<tr>
				<th>資 本 金</th>
				<td>5,000,000円</td>
			</tr>
			<tr>
				<th>主要銀行</th>
				<td>三菱東京UFJ銀行　他</td>
			</tr>
			<tr>
				<th>ホームページURL</th>
				<td>http://s-r-m.jp/</td>
			</tr>
			<tr>
				<th>事業内容</th>
				<td>
					木製浴槽製造販売・住宅設備機器販売<br />
					浴槽入替工事及び付帯工事
				</td>
			</tr>
			<tr>
				<th>取締役CTO（技術責任者）</th>
				<td>田中英樹</td>
			</tr>
			<tr>
				<th>取締役CFO（財務責任者）</th>
				<td>青木伸展</td>
			</tr>
			<tr>
				<th>帝国データ 企業コード</th>
				<td>873008190</td>
			</tr>
			<tr>
				<th>許可・登録</th>
				<td>
					一級建築士事務所　第59399号<br />
					建設業許可（般-25）第106784号<br />
					宅建業許可（1）第22574号
				</td>
			</tr>				
		</table><!-- ./end company-table -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">アクセス</h3>
		<div id="company_map">
			<iframe width="760" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;geocode=&amp;q=%E5%90%8D%E5%8F%A4%E5%B1%8B%E5%B8%82%E6%98%AD%E5%92%8C%E5%8C%BA%E7%A6%8F%E6%B1%9F2-9-33&amp;aq=&amp;sll=35.145846,136.91186&amp;sspn=0.011387,0.021136&amp;gl=VN&amp;ie=UTF8&amp;hq=&amp;hnear=2+Chome-9-33+Fukue,+Sh%C5%8Dwa-ku,+Nagoya-shi,+Aichi-ken+466-0059,+Nh%E1%BA%ADt+B%E1%BA%A3n&amp;t=m&amp;z=14&amp;ll=35.145846,136.91186&amp;output=embed"></iframe>
		</div><!-- ./company_map -->
	</div><!-- ./primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->
	
<?php get_footer(); ?>