<section id="slider">
	<div class="slider-content wrapper">
		<div class="bxslider">
			<div class="item"><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/mainslide_01.jpg" /></div>
			<div class="item"><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/mainslide_02.jpg" /></div>			
		</div>
	</div>
</section>