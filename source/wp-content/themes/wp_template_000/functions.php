<?php

function replaceImagePath($arg) {
$content = str_replace('"img/', '"' . get_bloginfo('template_directory') . '/img/', $arg);
return $content;
}
add_action('the_content', 'replaceImagePath');

function custom_list_categories($categories, $parent = 0, $depth = 0, $current_depth = 1, $show_count = 0)
{
	$output = '';
	if($depth != 0 && $current_depth > $depth){
		return $output;
	}
	if($current_depth > 1){
		$output .= '<ul>';
	}
	foreach($categories as $category){		
		if($category->parent == $parent && $category->cat_ID != 1){
			$output .= '<li>';
			$output .= '<a href="' . get_category_link( $category->cat_ID ) . '">';
			$output .= $category->name . '<span class="post_counts">';
			if($show_count) $output .= '(' . $category->count . ')';
			$output .= "</span>";
			$output .= '</a>';
			// find child
			$output .= custom_list_categories($categories,$category->cat_ID, $depth, $current_depth+1);
			$output .= '</li>';
		}
	}
	if($current_depth > 1){
		$output .= '</ul>';
	}
	return $output;
}

//1. Register Theme Features
function custom_theme_features()  {
	/*  Theme Support : 
		http://generatewp.com/theme-support/
		http://codex.wordpress.org/Function_Reference/add_theme_support
	--------------------------------------*/

	global $wp_version;

	// Add theme support for Automatic Feed Links
	if ( version_compare( $wp_version, '3.0', '>=' ) ) :
		add_theme_support( 'automatic-feed-links' );
	else :
		automatic_feed_links();
	endif;

	// Add theme support for Post Formats
	//$formats = array( 'image');
	//add_theme_support( 'post-formats', $formats );	

	// Add theme support for Featured Images
	add_theme_support( 'post-thumbnails', array( 'post', 'page', 'voice','work' ) );

	//	add_image_size( 'work_size', 370, 200, 1);

	// Add theme support for Translation
	load_theme_textdomain( 'text_domain', get_template_directory() . '/language' );	
}

// Hook into the 'after_setup_theme' action
add_action( 'after_setup_theme', 'custom_theme_features' );


function theme_name_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}
	
	global $page, $paged;

	// Add the blog name
	$title .= ' - ' . get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'theme_name_wp_title', 10, 2 );


class CustomPostType{
// Register Custom Post Type
	var $post_name;
	var $post_slug;
	var $supports = array();
	var $include_taxonomy = false;
	
	public function __construct($post_name, $post_slug , $include_taxonomy = true, $supports = array( 'title', 'editor', 'thumbnail') )
	{
		$this->post_name = $post_name;
		$this->post_slug = $post_slug;
		$this->supports = $supports;
		$this->include_taxonomy = $include_taxonomy;
		// Hook into the 'init' action
		add_action( 'init', array($this,'init_post'), 0 );
		if($this->include_taxonomy){
			add_action( 'init', array($this,'init_post_taxonomies'), 0 );
		}
	}	

	public function init_post() {

		$labels = array(
			'name'                => _x( $this->post_name, 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( $this->post_name, 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( $this->post_name, 'text_domain' ),
			'parent_item_colon'   => __( $this->post_name . ' root', 'text_domain' ),
			'all_items'           => __( 'All ' . $this->post_name, 'text_domain' ),
			'view_item'           => __( 'View ' ),
			'add_new_item'        => __( 'Add ' . $this->post_name, 'text_domain' ),
			'add_new'             => __( 'New ' . $this->post_name, 'text_domain' ),
			'edit_item'           => __( 'Edit ' . $this->post_name, 'text_domain' ),
			'update_item'         => __( 'Update ' . $this->post_name, 'text_domain' ),
			'search_items'        => __( 'Search ' . $this->post_name, 'text_domain' ),
			'not_found'           => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'  => __( 'Not found in trash', 'text_domain' ),
		);
		if($this->include_taxonomy){
			$taxonomies = array( $this->post_slug );
		}else{
			$taxonomies = array();
		}
		$args = array(
			'label'               => __( $this->post_name, 'text_domain' ),
			'description'         => __( $this->post_name, 'text_domain' ),
			'labels'              => $labels,
			'supports'            => $this->supports,
			'taxonomies'          => $taxonomies,
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			//'menu_position'       => 5,
			//'menu_icon'           => 'post',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( $this->post_slug, $args );
	}


	function init_post_taxonomies() {
	  // Add new "Locations" taxonomy to Posts
	  register_taxonomy( 'cat-'.$this->post_slug , array( $this->post_slug ), array(
	    // Hierarchical taxonomy (like categories)
	    'hierarchical' => true,
	    // This array of options controls the labels displayed in the WordPress Admin UI
	    'labels' => array(
	      'name' => _x( 'Category ' . $this->post_name, 'taxonomy general name' ),
	      'singular_name' => _x( 'Category ' . $this->post_name, 'taxonomy singular name' ),
	      'search_items' =>  __( 'Search Category ' . $this->post_name ),
	      'all_items' => __( 'All Category ' . $this->post_name ),
	      'parent_item' => __( 'Category Parent ' . $this->post_name ),
	      'parent_item_colon' => __( 'Category Parent ' . $this->post_name . ':' ),
	      'edit_item' => __( 'Edit Category ' . $this->post_name ),
	      'update_item' => __( 'Update Category ' . $this->post_name ),
	      'add_new_item' => __( 'Add Category ' . $this->post_name ),
	      'new_item_name' => __( 'New Category ' . $this->post_name),
	      'menu_name' => __( 'Category ' . $this->post_name ),
	    ),	    
	    // Control the slugs used for this taxonomy
	    'rewrite' => array(
	      'slug' => 'cat-'.$this->post_slug, // This controls the base slug that will display before each term
	      //'with_front' => false, // Don't display the category base before "/$post_slug/"
	      'hierarchical' => true // This will allow URL's like "/$post_slug/boston/cambridge/"
	    ),
	  ));
	}	
}
// New custom post type 
$custom_post_news = new CustomPostType('ニュース','news', true);

// New custom post type 
$custom_post_voice = new CustomPostType('サイト利用者の声','voice', true);

// New custom post type 
$custom_post_work = new CustomPostType('工事実績','work', true,array('title','editor','thumbnail'));

// post thumbnail
add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 5 );


function my_post_image_html( $html, $post_id, $post_thumbnail_id, $size , $attr ='' ) {
	$post = get_post( $post_id );
	if(empty($html)){		
		// base of post type		
		
		if($post->post_type == 'work'){
			$html = get_bloginfo( 'template_url', false) . "/img/content/work_content_img_{$size}.jpg";
			$html = '<img src="' . $html . '" title="' . $post->post_title . '" />';
		}			
	}
	if($post->post_type == 'work'){ return $html; }	
  	$html = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_the_title( $post_id ) ) . '">' . $html . '</a>';
  	return $html;
}



remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action( 'wp_head','adjacent_posts_rel_link_wp_head', 10);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action( 'wp_head','wp_shortlink_wp_head',10, 0 );

?>