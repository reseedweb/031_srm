 <!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        

        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.vticker.min.js" type="text/javascript"></script>        		
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->

            <section id="top"><!-- begin top -->                
                <div class="wrapper top_content clearfix">
                    <h1 class="top-text">店舗、事務所や飲食店などの原状回復、店舗解体なら原状回復おまかせ.com。</h1>
                    <ul class="top-links">
                        <li><a href="<?php bloginfo('url'); ?>/partner">パートナー業者募集</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/flow">工事の流れ</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
                    </ul>
                </div>                
            </section><!-- end top -->            
 
            <header><!-- begin header -->
                <div class="wrapper">
                    <div class="header_content clearfix">            
                        <div class="logo">
                            <a href="<?php bloginfo('url'); ?>/"><img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" /></a>
                        </div><!-- ./logo -->
						<div class="header-info">
							<div class="header-info1">
								<ul>
									<li><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/header_content_img1.jpg" /></li>
									<li><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/header_content_img2.jpg" /></li>
									<li><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/header_content_img3.jpg" /></li>
								</ul>								
							</div>
							
							<div class="header-info1 header-info2">
								<ul>
									<li><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" /></li>
									<li><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/header_msg.png" /></li>									
								</ul>								
							</div>						
						</div><!-- ./header -->
                        
                    </div> <!-- ./header_content  -->              
                </div><!-- ./wrapper -->
            </header><!-- end header -->           	
            
			<?php if(is_home()) : ?>
				<section id="slider-navi">
					<?php get_template_part('part','slider'); ?>
					<nav>
						<div id="gNavi" class="wrapper">
							<ul class="dynamic clearfix">
								<li><a href="<?php bloginfo('url'); ?>/about">原状回復とは</a></li>
								<li class="item-second"><a href="<?php bloginfo('url'); ?>/flow">工事を<br />安くする方法</a></li>
								<li><a href="<?php bloginfo('url'); ?>/point">工事のポイント</a></li>
								<li><a href="<?php bloginfo('url'); ?>/select">業者の選び方</a></li>                        
								<li><a href="<?php bloginfo('url'); ?>/work">工事実績</a></li>
								<li><a href="<?php bloginfo('url'); ?>/voice">サイト利用者の声</a></li>
								<li><a href="<?php bloginfo('url'); ?>/estimate"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/navi-last.jpg" /></a></li>						
							</ul>
						</div>
					</nav>
				</section>                
			<?php else :?>
				<section id="top-navi">
					<nav>
						<div id="gNavi" class="wrapper">
								<ul class="dynamic clearfix">
								<li><a href="<?php bloginfo('url'); ?>/about">原状回復とは</a></li>
								<li class="item-second"><a href="<?php bloginfo('url'); ?>/flow">工事を<br />安くする方法</a></li>
								<li><a href="<?php bloginfo('url'); ?>/point">工事のポイント</a></li>
								<li><a href="<?php bloginfo('url'); ?>/select">業者の選び方</a></li>                        
								<li><a href="<?php bloginfo('url'); ?>/work">工事実績</a></li>
								<li><a href="<?php bloginfo('url'); ?>/voice">サイト利用者の声</a></li>
								<li><a href="<?php bloginfo('url'); ?>/estimate"><img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/navi-last.jpg" /></a></li>						
							</ul>
						</div>
					</nav>
				</section>				
            <?php endif; ?>
            
            <section id="content"><!-- begin content -->
                <div class="wrapper two-cols-left clearfix"><!-- begin two-cols -->
                    <main class="primary"><!-- begin primary -->
       