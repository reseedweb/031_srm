<div class="primary-row clearfix" id="form-order"><!-- begin primary-row -->
	<img alt="top" src="<?php bloginfo('template_url'); ?>/img/content/top_content_img6.png" />
	<div class="top-content-info3">
		<?php echo do_shortcode('[contact-form-7 id="356" title="Order"]') ?>
			<div style="display:none;">
				<select id="_areas_content">
     				<optgroup label=”愛知県”>
						<option value=”名古屋”>名古屋</option>
						<option value=”豊橋市”>豊橋市</option>
						<option value=”岡崎市”>岡崎市</option>
						<option value=”一宮市”>一宮市</option>
						<option value=”瀬戸市”>瀬戸市</option>
						<option value=”半田市”>半田市</option>
						<option value=”春日井市”>春日井市</option>
						<option value=”豊川市”>豊川市</option>
						<option value=”津島市”>津島市</option>
						<option value=”碧南市”>碧南市</option>
						<option value=”刈谷市”>刈谷市</option>
						<option value=”豊田市”>豊田市</option>
						<option value=”安城市”>安城市</option>
						<option value=”西尾市”>西尾市</option>
						<option value=”蒲郡市”>蒲郡市</option>
						<option value=”犬山市”>犬山市</option>
						<option value=”常滑市”>常滑市</option>
						<option value=”江南市”>江南市</option>
						<option value=”小牧市”>小牧市</option>
						<option value=”稲沢市”>稲沢市</option>
						<option value=”新城市”>新城市</option>
						<option value=”東海市”>東海市</option>
						<option value=”大府市”>大府市</option>
						<option value=”知多市”>知多市</option>
						<option value=”知立市”>知立市</option>
						<option value=”尾張旭市”>尾張旭市</option>
						<option value=”高浜市”>高浜市</option>
						<option value=”岩倉市”>岩倉市</option>
						<option value=”豊明市”>豊明市</option>
						<option value=”日進市”>日進市</option>
						<option value=”田原市”>田原市</option>
						<option value=”愛西市”>愛西市</option>
						<option value=”清須市”>清須市</option>
						<option value=”北名古屋”>北名古屋</option>
						<option value=”弥富市”>弥富市</option>
						<option value=”みよし市”>みよし市</option>
						<option value=”あま市”>あま市</option>
						<option value=”長久手市”>長久手市</option>
 					</optgroup>
					<optgroup label=”三重県”>
						<option value=”津市”>津市</option>
						<option value=”四日市市”>四日市市</option>
						<option value=”伊勢市”>伊勢市</option>
						<option value=”松阪市”>松阪市</option>
						<option value=”桑名市”>桑名市</option>
						<option value=”鈴鹿市”>鈴鹿市</option>
						<option value=”名張市”>名張市</option>
						<option value=”尾鷲市”>尾鷲市</option>
						<option value=”亀山市”>亀山市</option>
						<option value=”鳥羽市”>鳥羽市</option>
						<option value=”熊野市”>熊野市</option>
						<option value=”いなべ市”>いなべ市</option>
						<option value=”志摩市”>志摩市</option>
						<option value=”伊賀市”>伊賀市</option>						
					</optgroup>
						<optgroup label=”岐阜県”>
						<option value=”岐阜市”>岐阜市</option>
						<option value=”大垣市”>大垣市</option>
						<option value=”高山市”>高山市</option>
						<option value=”多治見市”>多治見市</option>
						<option value=”関市”>関市</option>
						<option value=”中津川市”>中津川市</option>
						<option value=”美濃市”>美濃市</option>
						<option value=”瑞浪市”>瑞浪市</option>
						<option value=”羽島市”>羽島市</option>
						<option value=”恵那市”>恵那市</option>
						<option value=”美濃加茂市”>美濃加茂市</option>
						<option value=”土岐市”>土岐市</option>
						<option value=”各務原市”>各務原市</option>
						<option value=”可児市”>可児市</option>
						<option value=”山県市”>山県市</option>
						<option value=”瑞穂市”>瑞穂市</option>
						<option value=”飛騨市”>飛騨市</option>
						<option value=”本巣市”>本巣市</option>
						<option value=”郡上市”>郡上市</option>
						<option value=”下呂市”>下呂市</option>
						<option value=”海津市”>海津市</option>						
					</optgroup>					
				</select>				
			</div>
	</div>
</div><!-- end primary-row -->