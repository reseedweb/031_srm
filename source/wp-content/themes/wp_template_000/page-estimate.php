<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">無料一括お見積り</h2>	
	<p>原状回復の工事業者に、私たちが見積り依頼させてもらいます。</p>
	</p>当サイトのサービスは“すべて無料”です。</p>		
	<div class="partner-info clearfix">
		<div class="partner-tel"><img src="<?php bloginfo('template_url'); ?>/img/content/partner_content_img.jpg" alt="partner" /></p></div>
		<div class="partner-text">
			<p>お電話でも対応させて頂きます。<br />
			どんな質問・要望でも
			</p>
			<p class="partner-text-clr">是非お気軽にお問合せください。</p>
		</div>
	</div>	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<?php echo do_shortcode('[contact-form-7 id="363" title="Estimate"]') ?>
			<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#zip').change(function(){					
						//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
						AjaxZip3.zip2addr(this,'','addr1','addr2');
					});
				});
			</script>	
	</div><!-- ./primary-row -->
	
	
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->

<?php get_footer(); ?>