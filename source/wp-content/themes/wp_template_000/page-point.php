<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">工事のポイント</h2>
	<div class="point-top_img"><img src="<?php bloginfo('template_url'); ?>/img/content/point_content_img1.png" alt="point" /></div>	
		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">対応が迅速</h3>
		<div class="message-right message-275 clearfix"><!-- begin message-275 -->
			<div class="image ">
				<img src="<?php bloginfo('template_url'); ?>/img/content/point_content_img2.jpg" alt="point" />
			</div><!-- ./image -->
			<div class="text">
				<p>まず、対応が迅速であること。これはとても重要なポイントです。</p>
				<p>いくら経験豊富・実績方な業者で安心でも、対応が遅く空室期間が長引いてしまうようでは減収を招き、アパート・マンション経営を圧迫してしまいます。</p>
				<p class="subpage-space">原状回復工事は、退去者が発生する度に必要となることから、いち早く対応してくれる業者を選定し、工事を行ってもらうことが重要です。</p>
			</div><!-- ./text -->
		</div><!-- end message-275 -->
	</div><!-- ./primary-row -->
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">居住者に対する細かい配慮</h3>
	<div class="message-right message-275 clearfix"><!-- begin message-275 -->
		<div class="image">
			<img src="<?php bloginfo('template_url'); ?>/img/content/point_content_img3.jpg" alt="point" />
		</div><!-- ./image -->
		<div class="text"> 
			<p>原状回復工事時に大切なのは、施工完了後の入居者の内見や、入居者後の居住の事を考慮した施工です。</p>
			<p>施工時・施工後に何をどこまtでやってくれるのか確認し、細かい配慮のできる業者を選定しなければなりません。</p>
		</div><!-- ./text -->
	</div><!-- end message-275 -->
</div><!-- ./primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">確かな技術力</h3>
	<div class="message-right message-275 clearfix"><!-- begin message-275 -->
		<div class="image">
			<img src="<?php bloginfo('template_url'); ?>/img/content/point_content_img4.jpg" alt="point" />
		</div><!-- ./image -->
		<div class="text"> 
			<p>オーナー様の要望に対し、どれだけきっちりと施工が出来るかも重要なポイントです。</p>
			<p>技術力の低い業者に依頼してしまうと、再施工等のトラブルを引き起こし、入居者との信頼関係構築に大きな支障をきたしますので、技術力・施工体制・チェック体制の整っている業者を選定しなければなりません。</p>
		</div><!-- ./text -->
	</div><!-- end message-275 -->
</div><!-- ./primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">施工主への経過報告</h3>
	<div class="message-right message-275 clearfix"><!-- begin message-275 -->
		<div class="image">
			<img src="<?php bloginfo('template_url'); ?>/img/content/point_content_img5.jpg" alt="point" />
		</div><!-- ./image -->
		<div class="text"> 
			<p>以外に忘れられていることが、施工主 様への進捗や問題発生したときの報告やその対処などの事後経過の報告です。</p>
			<p class="subpage-space">原状回復の工事の期間は、工事の規模や建物によってもちろん異なってきますが最短で3日で終わります。</p>
			<p>期間に関係なく、経過を報告し、工事の中身・経過を把握することも重要です。</p>
		</div><!-- ./text -->
	</div><!-- end message-275 -->
</div><!-- ./primary-row -->
<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->
<?php get_footer(); ?>