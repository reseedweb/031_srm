<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">パートナー業者募集</h2>	
	<p>パートナー業者登録をご希望される方は「パートナー登録依頼フォーム」へのご記入をお願いいたします。</p>
	</p>弊社にて依頼を確認しましたら、その後、審査を経てパートナー契約を締結させて頂く流れとなります。</p>
	<div class="partner-info clearfix">
		<div class="partner-tel"><img src="<?php bloginfo('template_url'); ?>/img/content/partner_content_img.jpg" alt="partner" /></p></div>
		<div class="partner-text">
			<p>お互いが協力し、共に成長していけるビジネスパートナー様を<br />
			随時募集しています。
			</p>
			<p class="partner-text-clr">是非お気軽にお問合せください。</p>
		</div>
	</div>		
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<?php echo do_shortcode('[contact-form-7 id="362" title="Partner"]') ?>
			<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#zip').change(function(){					
						//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
						AjaxZip3.zip2addr(this,'','address');
					});
				});
			</script>	
	</div><!-- ./primary-row -->
	
	
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->

<?php get_footer(); ?>