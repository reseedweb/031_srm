<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">工事のポイント</h2>
	<p>「原状回復おまかせ.com」で施工を行って戴いたお客様の生の声を ご紹介いたします。</p>
	<p>	
		工事後の正直な気持ちを記入して戴くよう、アンケートを実施しており、 お客様から戴いた声を今後の施工に反映し、<br />
		お客様により満足頂けるよう日々精進しております。
	</p>
	<p>
		ここでは、お客様から戴いたアンケート一部分掲載しておりますので、じっくりとご覧戴き、 皆様の今後のご参考に<br />
		なれば 幸いと思っております。
	</p>
	<?php	
	$voice_terms = get_terms('cat-voice',array(
		'hide_empty'    => false,
		'parent' => 0,
		'orderby' => 'slug', 
		'order'   => 'desc',    
	));
	?>
	<?php		
	$voice_term_count = 0;
	foreach($voice_terms as $voice_term) :
		$voice_term_count = $voice_term_count + 1; 
	?>
	<?php	
		$posts = get_posts( array(
		'post_type'=> 'voice',		 
		'posts_per_page' => 3,
		'paged' => get_query_var('paged'),		
		'tax_query' => array(
		array(
			'taxonomy' => 'cat-voice', 
			'field' => 'term_id', 			
			'terms' => $voice_term->term_id)),
			'order' => 'desc',
		));
	?>
	<?php $i = 0;?>
	<?php foreach($posts as $p):	?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="voice-content">
			<div class="voice-title clearfix">
				<h3 class="title-left"><?php echo $p->post_title; ?></h3>
				<h3 class="title-right">施工箇所： <?php echo $voice_term->name; ?></h3>
			</div>				
			<div class="voice-info">
				<div class="voice-img">
					<?php echo get_the_post_thumbnail($p->ID,'large'); ?>
				</div>								
				<h4 class="voice-post-title"><?php echo $p->title; ?></h4>
				<?php echo apply_filters('the_content', $p->post_content ); ?>	
			</div>
		</div>
	</div><!-- end primary-row -->	
	
	<?php endforeach; ?>
	<?php endforeach; ?>
	<div class="primary-row text-center">
	    <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
	</div>
	<?php wp_reset_query(); ?>
		

</div><!-- end primary-row -->	
	
<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>