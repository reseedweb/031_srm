<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">業者の選び方</h2>
	<div class="flow-content-info clearfix"><!-- begin flow-content-info -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.png" alt="flow" />
		<div class="flow-info1"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				お問い合わせ・ヒアリング
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>お見積もりフォームより、お客様情報のご登録をお願いいたします。</p>
				<p>
					お客様情報のご登録が完了しましたら、工事業者がエリアや物件内容、スケジュールな<br />
					どを確認しまして、お見積もりの参加を判断いたします。</p>
				<p>その後、弊社スタッフから連絡が入りますので、それまでお待ちください。</p>
				<p class="pt20">
					また電話でも受けつけております。受付は日本全国24時間365日体制ですので、<br />
					突然のご依頼でも結構です。お時間を問わずコールセンターへご連絡いただけますので、<br />
					ご安心ください。
				</p>
			</div><!-- ./flow-info-text -->	
			<div class="flow-contact">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_contact.jpg" alt="flow" />
				</a> 
			</div><!-- ./flow-contact -->
		</div><!-- ./flow-info1 -->
	</div><!-- end flow-content-info -->
	
	<div class="flow-content-info clearfix"><!-- begin flow-content-info1 -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.png" alt="flow" />
		<div class="flow-info2"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				現場の調査と打ち合わせ
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>
					お電話やメールフォームでのご相談後に無料で<br />
					現地調査を行い、現場状況を実際に調査・<br />
					確認させていただきます。
				</p>
				<p>	
					建物の状況や間取り、老朽化具合や設備の<br />
					解体など詳しく全て見てくれるので、<br />
					この現場調査である程度のイメージを<br />
					作ることが出来ます。
				</p>
			</div><!-- ./flow-info-text -->				
		</div><!-- ./flow-info2 -->
	</div><!-- end flow-content-info -->
	
	<div class="flow-content-info clearfix"><!-- begin flow-content-info1 -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.png" alt="flow" />
		<div class="flow-info2"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				見積書の提出
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>
					各工事業者からお見積もりを提示させて<br />
					いただきます。
				</p>
				<p>
					各社出そろいましたら内容、金額を確認の上、<br />
					ご検討をお願いいたします。
				</p>
				<p> 
					ご不明な点がありましたら、<br />
					お気軽にお問い合わせください。
				</p>
			</div><!-- ./flow-info-text -->				
		</div><!-- ./flow-info2 -->
	</div><!-- end flow-content-info -->
	
	<div class="flow-content-info clearfix"><!-- begin flow-content-info1 -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.png" alt="flow" />
		<div class="flow-info2"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				見積書の検討
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>
					ご提示させていただいたお見積りと<br />
					お客様のご希望を踏まえ、お見積りのご検討を<br />				
					お願いいたします<br />
					また、お見積もりにご納得頂けない場合は、<br />
					もちろんキャンセルも可能です。
				</p>
				<p>
					（キャンセルに一切料金は発生しませんので、<br />
					ご安心ください。）
				</p>
			</div><!-- ./flow-info-text -->				
		</div><!-- ./flow-info2 -->
	</div><!-- end flow-content-info -->
	
	<div class="flow-content-info clearfix"><!-- begin flow-content-info1 -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.png" alt="flow" />
		<div class="flow-info2"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				契　約
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>
					お見積りに納得いただけましたら、本契約を<br />
					お願いいたします。
				</p>
				<p>
					お客様のご都合に合わせて、作業日を決め着<br />
					工に入っていきます。
				</p>				
			</div><!-- ./flow-info-text -->				
		</div><!-- ./flow-info2 -->
	</div><!-- end flow-content-info -->
	
	<div class="flow-content-info clearfix"><!-- begin flow-content-info1 -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.png" alt="flow" />
		<div class="flow-info2"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				着　工
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>
					原状回復専門の職人との綿密な打ち合わせ後、<br />
					着工致します。施工期間中は、工事内容の確認・<br />
					報告を随時行い、施工内容の中間検査も定期的に<br />
					行います。
				</p>				
			</div><!-- ./flow-info-text -->				
		</div><!-- ./flow-info2 -->
	</div><!-- end flow-content-info -->
	
	<div class="flow-content-info clearfix"><!-- begin flow-content-info1 -->
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img7.png" alt="flow" />
		<div class="flow-info2"><!-- begin flow-info1 -->
			<div class="flow-info-title">
				施工完了
			</div><!-- ./flow-info-title -->
			<div class="flow-info-text">
				<p>
					完成後にしっかりとお客様自身に確認していただき、<br />
					施工終了・完工となります。
				</p>
				<p>
					気になる点があった場合、工事業者へ<br />
					お気軽にお申し付けください。
				</p>
				<p>
					また、伝えにくい場合などがありましたら、<br />
					弊社のスタッフがサポート対応させて<br/>
					頂きますので、どうぞお気軽にご相談ください。
				</p>				
			</div><!-- ./flow-info-text -->				
		</div><!-- ./flow-info2 -->
	</div><!-- end flow-content-info -->
</div><!-- emd primary-row -->

<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->

<?php get_footer(); ?>