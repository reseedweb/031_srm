<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">工事を安くする方法</h2>
	<p>原状回復の費用は、誰しも安く抑えたいところです。</p>
	<p>一般的に原状回復は、管理会社や不動産会社に任せてしまう人が多いです。</p>
	<p>管理会社や不動産会社に任せてしまうと原状回復の費用が予想以上に高くなるケースが多々あります。</p>
	<p class="subpage-space">原状回復の費用を安くおさえるなら、下記の点が重要です。</p>
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="price-content-info1"><!-- begin price-content-info2 -->
			<div class="price-info1-text">
				<p>自社施工の業者を選ぶ</p>
				<p class="pt20">複数の業者から見積りを取る</p>
			</div><!-- ./price-info1-text -->
		</div><!-- end price-content-info2 -->
	</div><!-- end primary-row -->
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">居住者に対する細かい配慮</h3>
	<div class="price-content-info2">
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img2.png" alt="price" /></p>
	</div>
	<p>ビルのオーナーから原状回復の発注を受けた指定業者が、自分たちでは工事をおこなわないことにあります。</p>
	<p>指定業者は下請け業者に発注します。さらに、下請け業者は孫請け業者に発注します。</p>
	<p>現場の職人や技能士に仕事が発注されるまでに各業者がマージンを抜いていくのです。</p>
	<p>結果的に、トータルコストが高額になります。</p>
</div><!-- ./primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h3 class="h3-title">複数の業者から見積もりを取る</h3>
	<div class="price-content-info3 clearfix"><!-- begin price-content-info3 -->
		<div class="image">
			<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img3.png" alt="price" />
		</div><!-- ./image -->
		<div class="text"> 
			<p>自社施工の業者を選ぶにしても、最初から１社だけに絞って原状回復の依頼をすることもあまりよくありません。</p>
			<p>その理由は、選定した業者が原状回復の相場よりも高いのか安いのか分からないからです。</p>
			<p>原状回復は、定価や相場が不明確で、建物や環境によって相場が異なってきます。</p>
			<p>ですので、施工業者によって原状回復の費用が大きく違ってくるケースは珍しくありません。</p>
			<p class="subpage-space">同じ施工内容にもかかわらず見積もり内容が2倍にもなるケースが多々あります。</p>
			<p>原状回復の工事は、1社だけの見積もりでなく複数の業者から見積もりを取り比較検討をすることが重要です。</p>
		</div><!-- ./text -->
	</div><!-- end price-content-info3 -->
</div><!-- ./primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h3 class="h3-title">原状回復費用の比較例</h3>
	<p>下記は実際の弊社で行った金額の参考例です。</p>
	<p>Ａ社，Ｂ社は弊社協力業者が出した見積金額、Ｃ社は主にお客様の指定業者が出した見積金額です。</p>
	<table class="price-table-info">
		<tr>	
			<th colspan=2 class="table-info-title">埼玉 事務所 19坪 原状回復費用</th>			
		</tr>
		<tr>	
			<th>A社</th>
			<td>35<span class="price-table-stext">万円</span></td>					
		</tr>
		<tr>			
			<th>B社</th>
			<td>45<span class="price-table-stext">万円</span></td>
		</tr>
		<tr>			
			<th>C社</th>
			<td>67<span class="price-table-stext">万円</span></td>
		</tr>
	</table>	
	<div class="price-table-text clearfix">32万円の差額</div>			
	<table class="price-table-info">
		<tr>	
			<th colspan=2 class="table-info-title">静岡県 飲食店店舗 30坪 スケルトン回復</th>			
		</tr>
		<tr>	
			<th>A社</th>
			<td>103<span class="price-table-stext">万円</span></td>					
		</tr>
		<tr>			
			<th>B社</th>
			<td>145<span class="price-table-stext">万円</span></td>
		</tr>
		<tr>			
			<th>C社</th>
			<td>175<span class="price-table-stext">万円</span></td>
		</tr>
	</table>	
	<div class="price-table-text clearfix">72万円の差額</div>	
	<p>これらの例からお分かりのように、工事費用は坪数だけでなく、工事内容によって大きく変わります。</p>
	<p>ぜひ一度、一括見積を依頼し、出来るだけ安く原状回復を行なって下さい。</p>
</div><!-- ./primary-row -->
<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->
<?php get_footer(); ?>