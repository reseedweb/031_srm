<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="contact bg" src="<?php bloginfo('template_url');?>/img/common/side_con_bg.jpg" />
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="contact" 
                src="<?php bloginfo('template_url');?>/img/common/side_con_btn.jpg" />
            </a>
        </div><!-- ./side-con-btn -->
		<div class="side-con-text1">
			<p class="side-con-title">シンエイ・リテイルマネジメント株式会社</p>
			<p class="side-con-title">名古屋オフィス（中部支社/本社）<br /></p>
			名古屋市昭和区福江2-9-33<br />
			navi白金4Ｆ<br />
			TEL：（代表）052-871-1781<br />
			FAX：052-871-1782			
		</div><!-- ./side-con-text1 -->
		<div class="side-con-text2">
			<p class="side-con-title">メンテナンスセンター「365日24ｈ対応」</p>
			TEL：052-508-4621
		</div><!-- ./side-con-text2 -->
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<a href="<?php bloginfo('url'); ?>/price">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_price.jpg" alt="side banner" />
	</a>
</div> 

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<a href="<?php bloginfo('url'); ?>/point">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_point.jpg" alt="side banner" />
	</a>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<a href="<?php bloginfo('url'); ?>/select">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_select.jpg" alt="side banner" />
	</a>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<aside id="sideNavi"><!-- begin sideNavi -->		
		<ul>
			<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-play"></i>ホーム</a></li>
			<li><a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-play"></i>原状回復とは</a></li>
			<li><a href="<?php bloginfo('url'); ?>/price"><i class="fa fa-play"></i>工事を安くする方法</a></li>
			<li><a href="<?php bloginfo('url'); ?>/point"><i class="fa fa-play"></i>工事のポイント</a></li>
			<li><a href="<?php bloginfo('url'); ?>/select"><i class="fa fa-play"></i>業者の選び方</a></li>
			<li><a href="<?php bloginfo('url'); ?>/work"><i class="fa fa-play"></i>工事実績</a></li>
			<li><a href="<?php bloginfo('url'); ?>/voice"><i class="fa fa-play"></i>サイト利用者の声</a></li>
			<li><a href="<?php bloginfo('url'); ?>/partner"><i class="fa fa-play"></i>パートナー業者募集</a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact"><i class="fa fa-play"></i>お見積り・お問い合わせはこちら</a></li>
			<li><a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-play"></i>工事の流れ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-play"></i>よくある質問</a></li>
			<li><a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-play"></i>運営会社</a></li>
			<li><a href="<?php bloginfo('url'); ?>/privacy"><i class="fa fa-play"></i>プライバシーポリシー</a></li>
			<li><a href="<?php bloginfo('url'); ?>/blog"><i class="fa fa-play"></i>ブログ</a></li>			
		</ul>
	</aside><!-- end sideNavi -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<a href="<?php bloginfo('url'); ?>/partner">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_partner.jpg" alt="side banner" />
	</a>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<a href="<?php bloginfo('url'); ?>/blog">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_blog.jpg" alt="side banner" />
	</a>
</div><!-- end sidebar-row -->

<div class="sidebar-row"><!-- begin sidebar-row -->
    <?php get_template_part('part','area'); ?>
</div>

