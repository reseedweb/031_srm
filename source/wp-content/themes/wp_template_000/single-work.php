<?php get_template_part('header'); ?>
<div class="primary-row clearfix">
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<div class="primary-row clearfix">		
		<h2 class="h2-title">工事実績</h2>
		<div class="work-detail-content">
			<ul class="bxslider">
				<li>
					<?php if( get_field('image_1') ): ?>
						<img src="<?php the_field('image_1'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('image_2') ): ?>
						<img src="<?php the_field('image_2'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('image_3') ): ?>
						<img src="<?php the_field('image_3'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('image_4') ): ?>
						<img src="<?php the_field('image_4'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
			</ul>

			<div id="bx-pager">
				<a data-slide-index="0" href="#">
					<?php if( get_field('image_1') ): ?>
						<img src="<?php the_field('image_1'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</a>
				<a data-slide-index="1" href="#">
					<?php if( get_field('image_2') ): ?>
						<img src="<?php the_field('image_2'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</a>
				<a data-slide-index="2" href="#">
					<?php if( get_field('image_3') ): ?>
						<img src="<?php the_field('image_3'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</a>
				<a data-slide-index="3" href="#">
					<?php if( get_field('image_4') ): ?>
						<img src="<?php the_field('image_4'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</a>
			</div>
			<div class="work-detail-info">
				<div class="work-text1"><?php the_content(); ?></div>
				<div class="work-text2">
					<p>社名	： <?php the_title(); ?></p>
					<p class="pt10">坪数	： <?php echo get_field('width', get_the_id()); ?></p>
				</div>
				<div class="work-text3">
					<p>概要	： <?php @the_terms(get_the_id(), 'cat-work'); ?></p>
					<p class="pt10">竣工	： <?php the_time('Y年m月'); ?></p>
				</div>
			</div>
		</div>  
	</div><!-- end primary-row -->    
	
	<div class="primary-row clearfix">	    
		<?php if( get_previous_post() ): ?>		
		<div class="work-detail-btn">
			<?php previous_post_link('%link', '« %title'); ?>
		</div>		
		<?php endif;?>		
	</div><!-- end primary-row -->
	<?php endwhile;endif; ?>
</div><!-- end primary-row -->
<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_template_part('footer'); ?>