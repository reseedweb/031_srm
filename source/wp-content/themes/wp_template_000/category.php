<?php get_template_part('header'); ?>
<div class="primary-row"><!-- begin primary-row -->
    <div id="category1"><!-- begin category1 -->
        <h2 class="h2-title"><?php single_cat_title('',true); ?></h2>
       <?php
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		//print_r($queried_object);
		?>
		<?php $posts = get_posts(array(
			'post_type'=> 'post',
			'posts_per_page' => get_query_var('posts_per_page'),
			'paged' => get_query_var('paged'),
				'tax_query' => array(
				array(
				'taxonomy' => $queried_object->taxonomy,
				'field' => 'term_id',
				'terms' => $term_id))
		));
		?>   
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>    
            <!-- do stuff ... -->
            <div class="primary-row"><!-- begin primary-row -->            
                <h3 class="h3-title"><?php the_title(); ?></h3>            
                <div class="post-row-content">                
                    <div class="post-row-meta">
                        <i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
                        <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                        <i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
                    </div><!-- ./post-row-meta -->
                    <div class="post-row-description">
						<?php the_excerpt(); ?>
					</div><!-- ./post-row-description -->
                    <div class="blog-btnrm">
						<a href="<?php the_permalink(); ?>">Read more</a>
					</div>               
                </div><!-- ./post-row-content -->
            </div><!-- ./ end primary-row -->
            <?php endwhile; ?>    
            <div class="primary-row">
                <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
            </div><!-- ./primary-row -->
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div><!-- end category1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->

<?php get_template_part('footer'); ?>