<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">原状回復とは</h2>	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="message-right message-283 clearfix"><!-- begin message-283 -->
			<div class="image ">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img1.jpg" alt="about" />
			</div><!-- ./image -->
			<div class="text">
				<p>店舗・オフィスの賃貸借契約では、テナント側は契約終了後、<br />つまり退去時に、原則として原状回復義務<br />
					（入居時の状態に戻すこと）を定められていることが<br />ほとんどです。</p>
				<p class="subpage-space">
					国土交通省が示す「原状回復をめぐるトラブルとガイドライン」<br />
					によると、原状回復は「賃借人の居住、使用により<br />
					発生した建物価値の減少のうち、賃借人の故意・過失、<br />
					善管注意義務違反、その他通常の使用を超えるような<br />
					使用による損耗・毀損を復旧すること」と定義されています。</p>

				<p class="subpage-space">つまり、<span class="text-clr">原状回復工事とは、借りたときの状態に戻す工事のこと</span><br />
					を指します。<br />
					また、ほとんどの場合、「原状回復義務」は借主側にあります。</p>
			</div><!-- ./text -->
		</div><!-- end message-283-->
		<p>原状回復の費用は物件によって様々で、相場が特に決まっておりません。</p>
		<p>しかし、同じ見積項目を複数の原状回復の業者へ見積りを依頼することで不当な金額を支払わずに済み、<br />
			工事金額を最小限に抑えることができます。</p>
	</div><!-- ./primary-row -->
	
	<div class="primary-row clearfix">
		<div class="about-content-list"><!-- begin about-content-list -->
			<div class="about-list-text">
				<ul>
					<li>初めての原状回復工事ですが、<br />どの業者を選べばいいかわからない</li>
					<li>できるだけ費用を節約したい</li>
					<li>業者の見積もり内容がよくわからない</li>
					<li>店舗・オフィスの退去で原状回復をしないといけない</li>
					<li>信頼できる、質の高い業者をさがしたい</li>
					<li>近隣の方に、迷惑をかけずにスムーズに工事をおこないたい</li>
				</ul>
			</div><!-- ./about-list-text -->
			<div class="about-text">
				<p>当サイトでは、無料で原状回復の見積もりを行っていただくことができます。</p>
				<p>サイトを利用しても一切費用はかかりません。</p>
				<p>複数の業者に見積もり依頼を出すので、比較・検討をしていただくことができます。</p>
			</div><!-- ./about-text -->
		</div><!-- end about-content-list -->
	</div><!-- ./primary-row -->
	
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">原状回復の費用が変動する理由</h3>
	<p>原状回復の費用は、建物の状態や地域や業者、また周辺状況などによって変わりますが、同じ業者であっても費用が<br />
		変わる場合があります。
	</p>
	<p>たとえば、繁忙期には高くなり、閑散期には安くなるといったことが挙げられます。</p>
</div><!-- ./primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">状況回復で借主が負担する範囲</h3>
	<table class="about-content-table"><!-- begin about-content-table -->
		<tr>
			<th></th>
			<td class="about-title1">賃貸人（貸主）の負担となるもの</td>
			<td class="about-title2">賃借人（借リ主）の負担となるもの</td>
		</tr><!-- ./tr -->
		<tr>
			<th class="about-img">			
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img3.jpg" alt="about" />				
			</th>
			<td/>
				<div class="about-sinfo1">
					<p><i class="fa fa-circle"></i>畳の裏返し、表替え</p>
					<p class="pt20"><i class="fa fa-circle"></i>フローリングのワックスがけ</p>
					<p class="pt20">
						<i class="fa fa-circle"></i>家具の設置による床、<br />
						&nbsp;&nbsp;&nbsp;カーペットのへこみ、設置跡<br />
					</p>
					<p class="pt20"><i class="fa fa-circle"></i>畳の変色、フローリングの色落ち</p>
				</div>				
			</td>
			<td>
				<p>
					<i class="fa fa-circle"></i>カーペットに飲み物などをこぼした<br />
					　ことによるシミ、カビ
				</p>
				<p class="pt20"><i class="fa fa-circle"></i>冷蔵庫下のサビ跡</p>
				<p class="pt20"><i class="fa fa-circle"></i>引越し作業等で生じた引っかきキズ</p>									
				<p class="pt20">
					<i class="fa fa-circle"></i>フローリングの色落ち<br />
					<span class="sfont">（賃借人の不注意で雨が吹き込んだことなどによるもの）</span>
				</p>
			</td>
		</tr><!-- ./tr -->
		
		<tr>
			<th class="about-img">	
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img4.jpg" alt="about" />
			</th>
			<td>
				<div class="about-sinfo2">
					<p>
						<i class="fa fa-circle"></i>テレビ、冷蔵庫等の<br />
						後部壁面の黒ずみ（電気ヤケ）
					</p>
					<p class="pt20"><i class="fa fa-circle"></i>壁に貼ったポスターや絵画の跡</p>
					<p class="pt20"><i class="fa fa-circle"></i>壁紙の画鋲、ピン等の穴</p>
					<p class="pt20">
						<i class="fa fa-circle"></i>エアコン（賃借人（借主）所有）設置による<br />
						&nbsp;&nbsp;&nbsp;壁のビス穴、跡<br />
					</p>
					<p class="pt20"><i class="fa fa-circle"></i>クロスの変色</p>
				</div>				
			</td>
			<td>
				<p>
					<i class="fa fa-circle"></i>賃借人（借り主）が日常の清掃を<br />
					&nbsp;&nbsp;&nbsp;怠ったための台所の油汚れ
				</p>
				<p class="pt20">
					<i class="fa fa-circle"></i>賃借人（借り主）が結露を放置したことで<br />
					&nbsp;&nbsp;&nbsp;拡大したカビ、シミ
				</p>
				<p class="pt20">
					<i class="fa fa-circle"></i>クーラーから水漏れし、<br />
					&nbsp;&nbsp;&nbsp;賃借人（借り主）放置したため壁が腐食
				</p>
				<p class="pt20"><i class="fa fa-circle"></i>タバコのヤニ汚れ、臭い</p>
				<p class="pt20"><i class="fa fa-circle"></i>壁等の釘穴、ネジ穴</p>									
				<p class="pt20">
					<i class="fa fa-circle"></i>賃借人（借り主）が天井に直接つけた<br />
					&nbsp;&nbsp;&nbsp;照明器具の跡
				</p>
				<p class="pt20"><i class="fa fa-circle"></i>落書き等の故意による毀損</p>													
			</td>
		</tr><!-- .tr -->
		
		<tr>
			<th class="about-img">	
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img5.jpg" alt="about" />
			</th>
			<td>	
				<div class="about-sinfo3">
					<p><i class="fa fa-circle"></i>専門業者による全体のハウスクリーニング</p>
					<p class="pt20"><i class="fa fa-circle"></i>エアコンの内部洗浄</p>													
				</div>				
			</td>			
			<td>				
				<p><i class="fa fa-circle"></i>ガスコンロ置き場、換気扇などの油汚れ、すす</p>
				<p class="pt20"><i class="fa fa-circle"></i>風呂、トイレ、洗面台の水垢、カビ等</p>									
				<p class="pt20">
					<i class="fa fa-circle"></i>日常の不適切な手入れもしくは<br />
					&nbsp;&nbsp;&nbsp;用法違反による設備の毀損
				</p>
			</td>
		</tr><!-- ./tr -->
	</table><!-- end about-content-table -->
</div><!-- ./primary-row -->


<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="top-box1-title">弊社の状況回復にかかる参考費用</h3>    
    <div class="message-group message-col138 message-classic"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img1.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">クロス貼替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->           
            </div><!-- end message-col -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img2.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">CF張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                 
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img3.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">カーペット</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img4.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">CF張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                     
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img5.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">畳・表替え</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img6.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">襖・張替・天袋張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->           
            </div><!-- end message-col -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img7.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">障子　張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                 
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img8.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">ウッドタイル</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img9.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">クリーニング</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                     
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img10.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">鍵のシリンダー交換</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img11.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">退去立ち会い</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->           
            </div><!-- end message-col -->          
        </div><!-- end message-row -->	
		
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">原状回復のトラブルを防ぐために</h3>
	<p>
		原状回復は退去後に発生するものですが、トラブルを未然に防ぐためには、<br />
		入居中の暮らしで住まい全体をどれだけ丁寧に扱うかに左右されます。
	</p>
	<p>突発的に起こってしまった事態では、自己判断や後延ばしをせず、管理会社や貸主に早急に連絡をすることが大切です。</p>
</div><!-- ./primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">原状回復おまかせ.comの原状回復工事</h3>
	<p>原状回復おまかせ.comの「原状回復事業」では、過去5,000件の原状回復工事をご紹介させていただいております。</p>
	<p>当社に原状回復工事をお任せいただければ、工事手配の手間が一切かかりません。</p>
	<p>
		もちろん、解約申し入れの受領から、オーナー様への通知、解約立会い代行、原状回復工事、リフォームまで、<br />
		一括して弊社にお任せいただける優良な業者をご紹介させていただきます。
	</p>	
</div><!-- ./primary-row -->

<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->

<?php get_footer(); ?>