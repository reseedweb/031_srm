<div class="region">
	<h2 class="area-top-title">対応エリア</h2>
	<ul>
	<?php
	if(class_exists('CustomRegion')){
		$custom_region = CustomRegion::get_instance();
		echo $custom_region->get_region_panel();	
	}
	?>
	</ul>
</div>    	
<script type="text/javascript">
$(document).ready(function(){
	$('.region > ul > li > ul > li > h3').prepend('<strong>[</strong>');
	$('.region > ul > li > ul > li > h3').append('<strong>]</strong>');
});
</script>