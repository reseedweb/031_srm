<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="contact bg" src="<?php bloginfo('template_url');?>/img/common/side_con_bg.jpg" />
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="contact" 
                src="<?php bloginfo('template_url');?>/img/common/side_con_btn.jpg" />
            </a>
        </div><!-- ./side-con-btn -->
		<div class="side-con-text1">
			<p class="side-con-title">シンエイ・リテイルマネジメント株式会社</p>
			<p class="side-con-title">名古屋オフィス（中部支社/本社）<br /></p>
			名古屋市昭和区福江2-9-33<br />
			navi白金4Ｆ<br />
			TEL：（代表）052-871-1781<br />
			FAX：052-871-1782			
		</div><!-- ./side-con-text1 -->
		<div class="side-con-text2">
			<p class="side-con-title">メンテナンスセンター「365日24ｈ対応」</p>
			TEL：052-508-4621
		</div><!-- ./side-con-text2 -->
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h2 class="sideBlog-title">最新の投稿</h2>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h2 class="sideBlog-title">最新の投稿</h2>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h4 class="sideBlog-title">最新の投稿</h4>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
