<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">よくある質問</h2>
	<p>「原状回復おまかせ.com」に寄せられる質問を以下にまとめました。</p>
	<p>その他の質問がある場合は、お気軽にお電話でお問い合わせ下さい。</p>	
		
	<div class="faq-content-list clearfix"><!-- begin faq-content-list -->
		<div class="faq-content-row clearfix">
			<h3 class="question">当サイト利用のメリットを教えてください</h3>
			<div class="answer">
				<p>当サイトでは、時間と手間をかけずに複数の原状回復の工事会社からの見積りを一度に取り寄せることができます。</p>
				<p>当サイトから依頼をしてもらうだけで、複数の原状回復の工事会社から見積りをうけることができます。</p>
				<p class="answer-space">そして、複数の見積りを比較したのちに「ココに原状回復を依頼したい。」と思われたところに依頼をすることができます。</p>
				<p>また、複数の工事会社から見積りを取り寄せると、依頼するところ以外には、お断りの連絡を入れなければいけません。そのお断りも当サイトが、あなたの代わりに連絡させてもらいますので、その手間も省くことができます。</p>
				<p>また比較をすることで、原状回復の業者間の競争になるので費用も通常より安くなります。これが、このサービスにおける最大のメリットです。</p>
				<p class="answer-space">提携業者も弊社の厳しい基準を通過した業者のみとなっていますので、その取捨・判断をする手間も省くことが出来ます。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->
		
		<div class="faq-content-row clearfix">
			<h3 class="question">なぜ工事費用を安くできるのですか。</h3>
			<div class="answer">
				<p>「原状回復おまかせ.com」の加盟提携業者は、オフィスや店舗の原状回復や内装解体工事を専門とした会社です。</p>
				<p>工事依頼はこれらの加盟専門業者とお客様との直接契約になりますので、無駄な中間コストを省いた最安値の見積りが可能になります。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">サイトを利用するのに、費用はかかりますか？</h3>
			<div class="answer">
				<p>いいえ。一切料金は発生しませんので、ご安心ししてご利用ください。</p>				
			</div>
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">お見積もり後、キャンセルしても大丈夫ですか？</h3>
			<div class="answer">
				<p>はい。もちろん大丈夫です。</p>
				<p>お見積もり後のキャンセルでも料金は一切発生しませんのでご安心ください。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">工期は何日くらいですか？</h3>
			<div class="answer">
				<p>約2週間前後が目安となります。</p>
				<p>しかし、内装設備、立地、建物形状、工事時間帯、貸主との契約内容などにより工期も変わってきます。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">店舗以外の原状回復工事・解体工事も対応できますか？</h3>
			<div class="answer">
				<p>もちろん、可能です。</p>
				<p>店舗に限らず、事務所や、ビル、マンションなど様々な工事に対応してきた実績がある企業様をご紹介させて頂きますのでご安心ください。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">原状回復工事とは何ですか？</h3>
			<div class="answer">
				<p>原状回復工事とは、借りたときの状態に戻す工事のことを指します。</p>
				<p>ただし、これは入居した時の状態に戻すことではありません。自然消耗などの経年変化によるもの以外が回復対象となります。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">しつこいセールスなどはありませんか？</h3>
			<div class="answer">
				<p>はい。ご紹介する工事業者や、弊社から一方的なセールスをすることはありません。</p>
				<p>ご安心ください。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
	</div><!-- end faq-content-list -->
</div><!-- end primary-row -->
<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row --> 
<?php get_footer(); ?>