<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">業者の選び方</h2>
	<div class="choice-content-info1 clearfix"><!-- begin choice-content-info1 -->
		<div class="choice-info1"><!-- begin choice-info1 -->
			<div class="choice-info-title">
				原状回復工事の<span class="choice-info-clr">実績</span>が豊富にある
			</div><!-- ./choice-info-title -->
			<div class="choice-info-text">
				<p>業者選びのひとつの目安としたいのが、実績です。</p>
				<p>実績が豊富にあるということは、それだけ地域から信頼され、そしてその信頼に応え続けられているということ。</p>
				<p>ホームページがある場合、施工実績や公式ブログなどをチェックしてください。</p>
				<p>実績が確認出来ない際は、実際に電話やメールで問合せてみることをおすすめします。</p>
				<p>担当者が細かい内容まで丁寧に説明できるか、よく見極めることが大切です。</p>
			</div><!-- ./choice-info-text -->			
		</div><!-- end choice-info1 -->
		
		<div class="choice-info2"><!-- begin choice-info2 -->
			<div class="choice-info-title">
				<span class="choice-info-clr">近隣・周辺環境</span>への<span class="choice-info-clr">配慮</span>できているか
			</div><!-- ./choice-info-title -->
			<div class="choice-info-text">
				<p>原状回復工事では、騒音・振動・ホコリなどが発生するため、ご近所へ迷惑をかけることになります。</p>
				<p>また他のテナントや管理会社の都合などで、さまざまな制限や配慮が必要となってきます。</p>
				<p>ですので、依頼主がトラブルにならないよう着工前挨拶を行なっているか、</p>
				<p>どのような近隣対応を行なっているかなどを、事前に確認しておくと良いかもしれません。</p>
			</div><!-- ./choice-info-text -->						
		</div><!-- end choice-info2 -->
		
		<div class="choice-info3"><!-- begin choice-info3 -->
			<div class="choice-info-title">
				<span class="choice-info-clr">説明・対応</span>の仕方
			</div><!-- ./choice-info-title -->
			<div class="choice-info-text">
				<p>言葉づかいや対応、説明内容のわかりやすさなどで見えてきます。</p>
				<p>あなたの質問にどのように答えるのか、一つ一つ丁寧に答えてくれるかどうか、納得のいく説明をしてくれるかどうかも悪質な業者かどうかを見分けるための助けとなります。</p>
			</div><!-- ./choice-info-text -->						
		</div><!-- end choice-info3 -->	
		
		<div class="choice-info4"><!-- begin choice-info4 -->
			<div class="choice-info-title">
				<span class="choice-info-clr">見積もり内容</span>は<span class="choice-info-cl">適切</span>か
			</div><!-- ./choice-info-title -->
			<div class="choice-info-text">
				<p>原状回復工事は施工範囲が広範におよぶ為、どこまでが施工範囲内でどこからが<br />施工範囲外なのかを明確にしておくことが必要です。ここが曖昧になっていると「クロスもやってくれる<br />と思ったのに」「水回りは別料金なんて聞いていない」といったトラブルに発展しがち。</p>
				<p>トラブルを未然に防ぐという意味でも、見積もりが明瞭なところを選ぶようにしましょう。</p>
				<p>不明点は見積もりの段階で明確化しておくことが重要です。</p>
			</div><!-- ./choice-info-text -->						
		</div><!-- end choice-info4 -->
		
		<div class="choice-info5"><!-- begin choice-info4 -->
			<div class="choice-info-title">
				<span class="choice-info-clr">アフターフォロー体制</span>は整っているか
			</div><!-- ./choice-info-title -->
			<div class="choice-info-text">
				<p>お引き渡しの後に、気になる点が出てくることも原状回復工事にはよくあります。</p>
				<p>そういう場合に、しっかり、迅速に対応してもらえる業者が安心です。</p>
			</div><!-- ./choice-info-text -->						
		</div><!-- end choice-info5 -->
		
	</div><!-- end choice-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<h3 class="h3-title">良い業者の特徴</h3>
	<p>原状回復工事を行う業者がたくさんあり、どこに依頼すべきか迷ってしまいがち。</p>
	<p>このページでは業者の選び方をご紹介しています。ぜひご参考ください。</p>
	<div class="choice-content-info2"><!-- begin choice-content-info2 -->
		<div class="choice-info2-text">
			<ul>
				<li>適正な価格対応が早い（特にクレーム対応）</li>
				<li>明朗会計な見積書</li>
				<li>知識・経験が豊富</li>
				<li>ルールに則って業務を行っている</li>
				<li>他には無い独自のサービスを打ち出している</li>
				<li>ホームページに過去の施工実績を掲載している</li>
			</ul>
		</div><!-- ./choice-info-text -->
	</div><!-- end choice-content-info2 -->
	<div class="choice-text-clr">
		一般的に、原状回復工事を行う業者は、大きく下記の3つに分類できます。
	</div>
	
	<div class="choice-content-info3 clearfix"><!-- begin choice-content-info3 -->
		<h4 class="choice-info3-title">建設会社・工務店</h4>
		<div class="choice-info3-text">
			<p>建設会社や工務店さんは工事の大半を下請け業者に外注する事が多く高くなる傾向があります。</p>
			<p>施工完了後の修繕工事や緊急対応などでも、自社施工体制が整っていないためスピードや対応力に欠ける点があります。</p>
		</div><!-- ./choice-info3-text -->
	</div><!-- end choice-content-info3 -->
	
	<div class="choice-content-info3 clearfix"><!-- begin choice-content-info3 -->
		<h4 class="choice-info3-title">職人・専門工事業者</h4>
		<div class="choice-info3-text">
			<p>
				個人や個人業者レベルで施工を行う会社や専門工事業者さんは、中間コストがほとんどかからないため<br />
				価格は安いことが多いです。反面それらの業者さんを調整する現場管理の手間が担当者様の負担になります。
			</p>
			<p>また、規模が小さいため、繁忙期などで工事が重なるとスピードや対応力という点が問題になることがあります。</p>
			<p>個人レベルになってくると説明・対応の仕方も注視して選択する必要があります。</p>
		</div><!-- ./choice-info3-text -->
	</div><!-- end choice-content-info3 -->
	
	<div class="choice-content-info4 clearfix"><!-- begin choice-content-info4 -->
		<h4 class="choice-info4-title">原状回復専門業者</h4>		
		<div class="choice-info4-text">
			<p>原状回復をメインに工事している業者だからこそ、低価格でスピーディーな対応が出来る体制を整えています。</p>
			<p>また入居後の不具合の対応や退去立会代行など、専門業者な らではの付帯サービスが充実していることが多いです。</p>
			<p>（業者ごとに差はあります）</p>
			<p>原状回復工事は季節による繁閑が大きく、春先の繁忙期には退去から次の入居までの期間が2～3日しかないというような状況もあります。このような時こそ、高い施工技術と柔軟な体制を兼ね備えた原状回復専門業者の強みが活かされる場面です。</p>
		</div>		
	</div><!-- end choice-content-info4 -->	
	<p class="choice-slogan">工事は、それに<span class="slogan-clr">特化</span>した<span class="slogan-clr">原状回復業者</span>を<span class="slogan-clr">オススメ</span>します!</p>
</div><!-- ./primary-row -->

<div class="primary-row clearfix">
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->
<?php get_footer(); ?>