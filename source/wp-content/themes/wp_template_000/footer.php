
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->
			<div class="top-scroll">
				<p>
					<a href="#form-order">
						<img src="<?php bloginfo('template_url'); ?>/img/common/top_scroll.png" alt="top_scroll" />							
					</a>
				</p>
			</div>
            <footer><!-- begin footer -->
				<div class="footer-info1">
					<div class="wrapper">
						<p>店舗、事務所や飲食店などの原状回復、店舗解体なら原状回復おまかせ.com。</p>
					</div>
				</div>
                <div class="footer-content clearfix"><!-- begin wrapper -->                    
					<div class="wrapper clearfix">
						<div class="footer-logo">
							<p><img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="footer_logo"/></p>							
						</div><!-- end footer-logo -->
						<div class="footer-tel">
							<p><img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="footer_tel"/></p>							
						</div><!-- end footer-col -->									
					</div><!-- end wrapper -->			
					<div class="footer-navi wrapper clearfix">
						<ul>
							<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
							<li><a href="<?php bloginfo('url'); ?>/about">原状回復とは</a></li>
							<li><a href="<?php bloginfo('url'); ?>/price">工事を安くする方法</a></li>
							<li><a href="<?php bloginfo('url'); ?>/point">工事のポイント</a></li>
							<li><a href="<?php bloginfo('url'); ?>/select">業者の選び方</a></li>
							<li><a href="<?php bloginfo('url'); ?>/work">工事実績</a></li>
							<li><a href="<?php bloginfo('url'); ?>/voice">サイト利用者の声</a></li>
							<li><a href="<?php bloginfo('url'); ?>/partner">パートナー業者募集</a></li>															
						</ul>
						<ul class="text-center">
							<li><a href="<?php bloginfo('url'); ?>/contact">お見積り・お問い合わせはこちら</a></li>
							<li><a href="<?php bloginfo('url'); ?>/flow">工事の流れ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/faq">よくある質問</a></li>
							<li><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
							<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
						</ul>
					</div>
				</div>                    
                <div class="footer-copyright clearfix"><!-- begin wrapper -->
                    Copyright © 2014 ************  All Rights Reserved.
                </div>                         
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">
            jQuery(document).ready(function(){                
                // bxslider
                jQuery('.bxslider').bxSlider({
                    pagerCustom: '#bx-pager',
                    pause : 2000,
                    auto : true,
                });       
				$('.top-order-table select[name="areas"]').html($("#_areas_content").html());		
				$('#example').vTicker('init',{
					speed: 800, 
					pause: 1500,
					showItems: 4,				
				});	
            }); 
        </script>                	
    </body>
</html>