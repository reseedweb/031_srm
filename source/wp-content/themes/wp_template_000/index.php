﻿<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->	
	<div class="top-content-info1"><!-- begin top-content-info1 -->
		<div class="top-info1-blog" id="example">
			<?php
				$queried_object = get_queried_object();
				//print_r($queried_object);
				?>
				<?php $posts = get_posts(array(
					'post_type'=> 'news',	
					'posts_per_page' => 50,					
					'paged' => get_query_var('paged'),									 
				));
				?>
			<ul>
			<?php $i = 0;?>				
			<?php foreach($posts as $p) :  ?>	
				<li class="item">                
					<div class="date"><?php the_time('Y/m/d'); ?></div>
					<div class="title"><?php echo $p->post_title; ?></div>                        
				</li>
			<?php endforeach; ?>
			<?php wp_reset_query(); ?>			
			</ul>
		</div><!-- ./top-blog -->
		<div class="top-info1-text">
			<ul>
				<li>当然ですが、近隣への配慮やマニフェストなどの<br/>法的に重要な書類の管理・提示もおこないます<br />ので、安心しておまかせ下さい。</li>
				<li>当サイトの利用料金は無料です。<br />もちろん見積もり料金に上乗せすることは<br />一切ありません。</li>
				<li>おかげさまで、年間1,000件以上の現状回復の依<br />頼を頂いております。お客様の笑顔を第一に、<br />価格・質の両面から、安心して工事をお受け<br/>できるように支援します。</li>
			</ul>
			<ul>
				<li>完了後の定期的なフォローはもちろんのこと、<br />着工前の連絡・施工中もお客様に経過写真・<br />レポートをお送りしています。</li>
				<li class="info-text">１つの業者に見積もりをお願い、そのまま工事を依頼してしまうのでは、費用はほとんど抑えられません。最低でも2社に見積もりを出してもらい、比較することが大事です。これを行うことで、100万円前後の工事で20～30万円は変わってきます。</li>
				<li>匿名で見積もりを業者に依頼するので、<br />個人情報が漏れたりする心配もありません。</li>
			</ul>
		</div><!-- ./top-info1-text -->
	</div><!-- end top-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info2"><!-- begin top-content-info2 -->
		<div class="top-info2-text">
			<ul>
				<li>初めての原状回復工事ですが、<br />どの業者を選べばいいかわからない</li>
				<li>できるだけ費用を節約したい</li>
				<li>業者の見積もり内容がよくわからない</li>
				<li>店舗・オフィスの退去で原状回復をしないといけない</li>
				<li>信頼できる、質の高い業者をさがしたい</li>
				<li>近隣の方に、迷惑をかけずにスムーズに工事をおこないたい</li>
			</ul>
		</div><!-- ./top-info-text -->
	</div><!-- end top-content-info2 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="top-box1-title">弊社の状況回復にかかる参考費用</h3>    
    <div class="message-group message-col138 message-classic"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img1.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">クロス貼替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->           
            </div><!-- end message-col -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img2.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">CF張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                 
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img3.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">カーペット</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img4.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">CF張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                     
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img5.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">畳・表替え</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img6.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">襖・張替・天袋張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->           
            </div><!-- end message-col -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img7.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">障子　張替</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                 
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img8.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">ウッドタイル</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img9.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">クリーニング</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                     
            </div><!-- end message-col -->
			<div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img10.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">鍵のシリンダー交換</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->                       
            </div><!-- end message-col -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box1_img11.jpg" alt="top" />
                </div><!-- end image -->    
                <div class="text">
                    <p class="box1-text">退去立ち会い</p>
					<p>840円 ～ ／㎡</p>
                </div><!-- end text -->           
            </div><!-- end message-col -->          
        </div><!-- end message-row -->	
		
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="top-box2-title">状況回復のお役立ち情報</h3>      
    <div class="message-group message-col370 message-classic"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col">
                <h3 class="title">原状回復とは</h3>
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_img1.jpg" alt="top" />
                </div><!-- ./image -->    
                <div class="text">
                    <p>	
						店舗やオフィスでテナントが退去する時に、<br />
						設備などを取り除いた状態で貸主に部屋を返す工事です。
					</p>
                </div><!-- ./text -->
				<div class="box2-btn">
					<a href="<?php bloginfo('url'); ?>/about">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_btn.jpg" alt="top" />
					</a>
				</div><!-- ./box-btn -->
            </div><!-- end message-col -->
            <div class="message-col">
                <h3 class="title">工事を安くする方法</h3>
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_img2.jpg" alt="top" />
                </div><!-- ./image -->    
                <div class="text">
                    <p>原状回復の費用は、誰しも安く抑えたいところです。</p>
					<p>ここでは、原状回復の費用をお安くする秘訣をお教えします。</p>
                </div><!-- ./text -->
				<div class="box2-btn">
					<a href="<?php bloginfo('url'); ?>/price">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_btn.jpg" alt="top" />
					</a>
				</div><!-- ./box-btn -->
            </div><!-- end message-col -->            
        </div><!-- end message-row -->
		
		 <div class="message-row clearfix"><!--message-row -->
            <div class="message-col">
                <h3 class="title">原状回復工事のポイント</h3>
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_img3.jpg" alt="top" />
                </div><!-- ./image -->    
                <div class="text">
                    <p>原状回復工事で知っておきたいポイントをご紹介します。</p>
                </div><!-- ./text -->
				<div class="box2-btn">
					<a href="<?php bloginfo('url'); ?>/point">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_btn.jpg" alt="top" />
					</a>
				</div><!-- ./box-btn -->
            </div><!-- end message-col -->
            <div class="message-col">
                <h3 class="title">業者の選び方</h3>
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_img4.jpg" alt="top" />
                </div><!-- ./image -->    
                <div class="text">
                    <p>原状回復業者はどうやって選べば良いのか?</p>
					<p>そんな疑問にお答えします。</p>
                </div><!-- ./text -->
				<div class="box2-btn">
					<a href="<?php bloginfo('url'); ?>/select">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_box2_btn.jpg" alt="top" />
					</a>
				</div><!-- ./box-btn -->
            </div><!-- end message-col -->            
        </div><!-- end message-row -->
		
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->    	
	<?php get_template_part('part','flow') ;?>
	<div class="primary-row clearfix">
		<p>
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img4.png" alt="top" />
			</a>   
		</p>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-top-title">原状回復、店舗解体なら原状回復おまかせ.com。</h2>  
    <div class="message-right message-258 clearfix">
        <div class="image">
            <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img5.jpg" alt="top" />
        </div>
        <div class="text">
			<p>原状回復おまかせ.comでは、複数の原状回復工事業者に無料で一括お問い合わせができます。原状回復おまかせ.comの「原状回復事業」では、現在までに5,000件の原状回復工事をご紹介させていただいております。</p>
			<p>また、現在も年間200件以上の原状回復工事を行っています。</p>
			<p class="top-space">「店舗の閉店で、いますぐ原状回復しないといけない」「オフィスの移転で、急ぎ原状回復で立ち退く必要がある」といった場合には当サイトにおまかせください。あなたの要望にあわせた最適な業者を最大3社までご紹介します。</p>
			<p>当サイトは業者に1社1社をお問い合わせする時間やコストを削減し複数の原状回復工事業者を様々な面で比較ができるので、効率よくお取引する業者を探せます。</p>
			<p class="top-space">当社では、原状回復の工事だけでなく、他の入居者やビルオーナー、そして管理会社とのやり取りまで一括して任せることができます。</p>
			<p class="top-space">プロの原状回復工事を、誰もがご納得いただける価格で工事を行なうのであれば原状回復おまかせ.comにおまかせください。</p>
        </div>
    </div><!-- end message-280 -->
</div><!-- end primary-row -->

<?php get_template_part('part','contact') ;?>

<?php get_footer(); ?>
