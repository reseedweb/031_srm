<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">プライバシポリシー</h2>			
	<div class="faq-content-list clearfix"><!-- begin faq-content-list -->
		<div class="faq-content-row clearfix">
			<h3 class="question">個人情報保護方針</h3>
			<div class="answer">
				<p>シンエイ・リテイルマネジメント株式会社（以下「当社」）は、以下のとおり個人情報保護方針を定め、個人情報保護の仕組みを構築し、全従業員に個人情報保護の重要性の認識と取組みを徹底させることにより、個人情報の保護を推進致します。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->
		
		<div class="faq-content-row clearfix">
			<h3 class="question">個人情報の利用目的</h3>
			<div class="answer">
				<p>お客さまからお預かりした個人情報は、当社からのご連絡や業務のご案内やご質問に対する回答として、電子メールや資料のご送付に利用いたします。</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">個人情報の第三者への開示・提供の禁止</h3>
			<div class="answer">
				<p>当社は、お客さまよりお預かりした個人情報を適切に管理し、次のいずれかに該当する場合を除き、個人情報を第三者に開示いたしません。</p>
				<p>
					・お客さまの同意がある場合<br />
					・お客さまが希望されるサービスを行なうために当社が業務を委託する業者に対して開示する場合<br/>
					・法令に基づき開示することが必要である場合
				</p>				
			</div>
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">個人情報の安全対策</h3>
			<div class="answer">
				<p>当社は、個人情報の正確性及び安全性確保のために、セキュリティに万全の対策を講じています。</p>				
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">法令、規範の遵守と見直し</h3>
			<div class="answer">
				<p>当社は、保有する個人情報に関して適用される日本の法令、その他規範を遵守するとともに、本ポリシーの内容を適宜見直し、その改善に努めます。</p>				
			</div><!-- ./answer -->
		</div><!-- end row1 -->	
		
		<div class="faq-content-row clearfix">
			<h3 class="question">お問い合せ</h3>
			<div class="answer">
				<p>当社の個人情報の取扱に関するお問い合せは下記までご連絡ください。</p>
				<p>				
					株式会社 樋口製作所<br />
					◆シンエイ・リテイルマネジメント株式会社<br />
					〒466-0059　名古屋市昭和区福江2-9-33　 navi白金4Ｆ<br />
					TEL：（代表）052-871-1781　 FAX：052-871-1782
				</p>
			</div><!-- ./answer -->
		</div><!-- end row1 -->			
	</div><!-- end faq-content-list -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<?php get_template_part('part','flow') ;?>
	<?php get_template_part('part','contact') ;?>
</div><!-- ./primary-row -->
<?php get_footer(); ?>